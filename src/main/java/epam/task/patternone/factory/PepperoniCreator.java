package epam.task.patternone.factory;

import epam.task.patternone.PizzaFuller.Pepperoni;
import epam.task.patternone.PizzaFuller.PizzaPrduct;

public class PepperoniCreator extends OrderCreator {
    @Override
    public PizzaPrduct createOrderFactory() {
        return new Pepperoni();
    }
}
