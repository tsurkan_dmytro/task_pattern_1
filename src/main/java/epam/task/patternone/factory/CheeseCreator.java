package epam.task.patternone.factory;

import epam.task.patternone.PizzaFuller.Cheese;
import epam.task.patternone.PizzaFuller.PizzaPrduct;
import epam.task.patternone.model.PizzaItem;

public class CheeseCreator extends OrderCreator {
    @Override
    public PizzaPrduct createOrderFactory() {
        return new Cheese(new PizzaItem(1));
    }
}
