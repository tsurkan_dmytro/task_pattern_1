package epam.task.patternone.factory;

import epam.task.patternone.PizzaFuller.Clam;
import epam.task.patternone.PizzaFuller.PizzaPrduct;

public class ClamCreator extends OrderCreator {
    @Override
    public PizzaPrduct createOrderFactory() {
        return new Clam();
    }
}
