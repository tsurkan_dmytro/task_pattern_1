package epam.task.patternone.factory;

import epam.task.patternone.PizzaFuller.PizzaPrduct;

public abstract class OrderCreator {

    public void createOrder() {
        PizzaPrduct pizza = createOrderFactory();
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
    }

    public abstract PizzaPrduct createOrderFactory();
}
