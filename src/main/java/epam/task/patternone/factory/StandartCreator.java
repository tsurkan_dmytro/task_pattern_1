package epam.task.patternone.factory;

import epam.task.patternone.PizzaFuller.Standart;
import epam.task.patternone.PizzaFuller.PizzaPrduct;

public class StandartCreator extends OrderCreator {
    @Override
    public PizzaPrduct createOrderFactory() {
        return new Standart();
    }
}
