package epam.task.patternone.enums;

public enum PizzaType {
    STADART(1), CALZONE(1.5), CHEESE(2.0), VEGGIE(1.4), CLAM(2.1), PEPPERONI(3.0);

    private PizzaType type;
    private final double pizzaBasePrice;

    public PizzaType getType() {
        return type;
    }

    PizzaType(double pizzaBase) {
        this.pizzaBasePrice = pizzaBase;
    }

    public double getPrice() {
        return this.pizzaBasePrice;
    }
}