package epam.task.patternone.enums;

public enum Sauces {
    MARINARA, PLUM_TOMATO, PESTO
}
