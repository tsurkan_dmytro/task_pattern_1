package epam.task.patternone;
import epam.task.patternone.factory.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainPizza {
    private Map<String,String> menu;
    private static Scanner input = new Scanner(System.in);

    private static OrderCreator creator;

    public MainPizza() {
        menu = new LinkedHashMap<>();

        menu.put("1", " 1 - Cheese pizza");
        menu.put("2", " 2 - Clam pizza");
        menu.put("3", " 3 - Pepperoni pizza");
        menu.put("4", " 4 - Standert pizza");
        menu.put("5", " 5 - Veggie pizza");
        menu.put("Q", " Q - exit");

    }


    public static void main(String[] args) {

        MainPizza mainPizza = new MainPizza();
        mainPizza.show();
    }

    static void configure(int i) {
        if (i==1) {
            creator = new CheeseCreator();
        } else if(i==2){
            creator = new ClamCreator();
        }else if(i==3){
            creator = new PepperoniCreator();
        }else if(i==4){
            creator = new StandartCreator();
        }else if(i==5){
            creator = new VeggieCreator();
        }else {
            System.out.println("None pizza");
        }
        runBusinessLogic();
        creator.createOrder();
        runBusinessLogic();
    }

    static void runBusinessLogic() {
        System.out.println("--------***--------");
    }

    private void outputMenu(){
        System.out.println("\n MENU:");

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;
        do {
            outputMenu();
            System.out.println();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
                configure(Integer.parseInt(keyMenu));
            }catch (Exception e){
                System.out.println("Wrong menu item");
            }
        }while (!keyMenu.equals("Q"));
    }
}
