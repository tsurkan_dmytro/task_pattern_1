package epam.task.patternone;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private Map<String,String> menu;
    private Map<String,Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Menu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Cheese pizza");
        menu.put("2", " 2 - Clam pizza");
        menu.put("3", " 3 - Pepperoni pizza");
        menu.put("4", " 4 - Standert pizza");
        menu.put("5", " 5 - Veggie pizza");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::cheesepizza);
        methodsMenu.put("2", this::clampizza);
        methodsMenu.put("3", this::pepperonipizza);
        methodsMenu.put("4", this::standertpizza);
        methodsMenu.put("5", this::veggiepizza);

    }

    private void outputMenu(){
        System.out.println("\n MENU:");

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;
        int kk = 1;
        do {
            outputMenu();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
              methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println("menu problem");
            }
        }while (!keyMenu.equals("Q"));
    }

    public int cheesepizza(){return 1;}
    public int clampizza(){return 222;}
    public int pepperonipizza(){return 3;}
    public int standertpizza(){return 4;}
    public int veggiepizza(){return 5;}

}