package epam.task.patternone.PizzaFuller;

import epam.task.patternone.enums.PizzaType;
import epam.task.patternone.model.PizzaItem;

public class Cheese implements PizzaPrduct {

    PizzaItem pizzaItem;

    public Cheese(PizzaItem pizzaItem) {
        this.pizzaItem = pizzaItem;
    }

    @Override
    public void prepare() {
        System.out.println("prepare cheese");
        pizzaItem.setPizzaType(PizzaType.CHEESE);
    }

    @Override
    public void bake() {
        System.out.println("bake cheese");
        pizzaItem.setTimeCreate("5 min");
    }

    @Override
    public void cut() {
        System.out.println("cut cheese for 5 slices");
    }

    @Override
    public void box() {
        System.out.println("box cheese pizza");
    }
}
