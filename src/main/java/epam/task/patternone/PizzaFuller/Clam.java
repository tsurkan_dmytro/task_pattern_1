package epam.task.patternone.PizzaFuller;

public class Clam implements PizzaPrduct {
    @Override
    public void prepare() {
        System.out.println("prepare Clam");
    }

    @Override
    public void bake() {
        System.out.println("bake Clam 3 min");
    }

    @Override
    public void cut() {
        System.out.println("cut Clam for 10 slices");
    }

    @Override
    public void box() {
        System.out.println("box Clam");
    }
}
