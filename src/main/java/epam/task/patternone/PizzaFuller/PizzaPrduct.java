package epam.task.patternone.PizzaFuller;

public interface PizzaPrduct {
    void prepare();
    void bake();
    void cut();
    void box();
}
