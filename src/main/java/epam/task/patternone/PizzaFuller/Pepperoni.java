package epam.task.patternone.PizzaFuller;

public class Pepperoni implements PizzaPrduct {
    @Override
    public void prepare() {
        System.out.println("prepare Pepperoni");
    }

    @Override
    public void bake() {
        System.out.println("bake Pepperoni for 3 min");
    }

    @Override
    public void cut() {
        System.out.println("cut Pepperoni for 4 slices");
    }

    @Override
    public void box() {
        System.out.println("box Pepperoni big box");
    }
}
