package epam.task.patternone.PizzaFuller;

public class Veggie implements PizzaPrduct {
    @Override
    public void prepare() {
        System.out.println("prepare Veggie");
    }

    @Override
    public void bake() {
        System.out.println("bake Veggie");
    }

    @Override
    public void cut() {
        System.out.println("cut Veggie");
    }

    @Override
    public void box() {
        System.out.println("box Veggie");
    }
}
