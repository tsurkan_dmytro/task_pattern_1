package epam.task.patternone.PizzaFuller;

public class Standart implements PizzaPrduct {
    @Override
    public void prepare() {
        System.out.println("prepare Standart");
    }

    @Override
    public void bake() {
        System.out.println("bake Standart");
    }

    @Override
    public void cut() {
        System.out.println("cut Standart");
    }

    @Override
    public void box() {
        System.out.println("box Standart");
    }
}
