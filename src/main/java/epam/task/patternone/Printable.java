package epam.task.patternone;

@FunctionalInterface
public interface Printable {
    public void print();
}
